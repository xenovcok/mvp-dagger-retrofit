package com.bmt.zicreative.dagger_mvp;

import android.app.Application;

import com.bmt.zicreative.dagger_mvp.di.AppComponent;
import com.bmt.zicreative.dagger_mvp.di.DaggerAppComponent;

public class BaseApplication extends Application {
    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder().application(this).build();
    }

    public AppComponent getComponent() {
        return component;
    }
}
