package com.bmt.zicreative.dagger_mvp.di;

import android.app.Application;

import com.bmt.zicreative.dagger_mvp.BaseApplication;
import com.bmt.zicreative.dagger_mvp.network.NetworkModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        NetworkModule.class,
        ActivityBuilder.class
})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance Builder application(Application application);
        AppComponent build();
    }

    void inject(BaseApplication app);
}
