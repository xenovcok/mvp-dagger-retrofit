package com.bmt.zicreative.dagger_mvp.network.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class ApiConfig {
    public static Gson GSON = new GsonBuilder()
            .create();
}
