package com.bmt.zicreative.dagger_mvp.network;

import com.bmt.zicreative.dagger_mvp.Utils.Constant;
import com.bmt.zicreative.dagger_mvp.network.config.ApiConfig;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    HttpLoggingInterceptor provideLoggingInterceptop() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.NONE);

        return logging;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient (HttpLoggingInterceptor logging) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().addNetworkInterceptor(logging);
        return httpClient.build();
    }

    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create(ApiConfig.GSON);
    }

    @Provides
    @Singleton
    Retrofit retrofit(OkHttpClient httpClient, GsonConverterFactory gson) {
        return new Retrofit.Builder()
                .baseUrl(Constant.BASE)
                .client(httpClient)
                .addConverterFactory(gson)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

}
